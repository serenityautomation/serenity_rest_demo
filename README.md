# Getting started with REST API testing with Serenity and Cucumber 6

## How to Install

On a local machine should be installed:

- Java JDK
- Maven
- Git Bash

In order to execute tests, execute the command bellow

- Execute Tests on 4 Threads with specific tag:

``mvn clean install -Dtags=@AllTests -Dparallel.tests=4``

## Custom Tags

- `@AllTests` - Used to execute all Tests.

## Features

- All Feature files are placed in /resource/feature

## Reporting
- From Maven Plugin Sections select ``serenity:aggregate``. After Execution a Report link should be generated. Open link in any browser and observe the results
## Jenkins Jobs

### API Tests

The base job, defined in Jenkins

- Url: example.com

How to run:

1. Click *Build with Parameters* button
3. Select *PARALLEL_TESTS* (Ex. 10)
4. Click *BUILD* button
5. Wait for results

## Endpoint Interactions

For the Test Demo was selected SkyScanner Api
- https://rapidapi.com/skyscanner/api/skyscanner-flight-search

Skyscanner Flight Search API Documentation
The Skyscanner API lets you search for flights & get flight prices from Skyscanner’s database of prices, as well as get live quotes directly from ticketing agencies.

### Get a list of places that match a query string.

- Method: GET
- Route: ``https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/UK/GBP/en-GB/?query='Stockholm'``

### Response Example:
```
"Places": [
        {
            "PlaceId": "STOC-sky",
            "PlaceName": "Stockholm",
            "CountryId": "SE-sky",
            "RegionId": "",
            "CityId": "STOC-sky",
            "CountryName": "Sweden"
        },
        {
            "PlaceId": "ARN-sky",
            "PlaceName": "Stockholm Arlanda",
            "CountryId": "SE-sky",
            "RegionId": "",
            "CityId": "STOC-sky",
            "CountryName": "Sweden"
        },
```

### Retrieve the currencies that we support.

- Method: GET
- Route: ``https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/currencies``

### Response Example:
```
"Currencies": [
{
"Code": "AED",
"Symbol": "AED",
"ThousandsSeparator": ",",
"DecimalSeparator": ".",
"SymbolOnLeft": true,
"SpaceBetweenAmountAndSymbol": true,
"RoundingCoefficient": 0,
"DecimalDigits": 2
},
{
"Code": "AFN",
"Symbol": "AFN",
"ThousandsSeparator": ",",
"DecimalSeparator": ".",
"SymbolOnLeft": true,
"SpaceBetweenAmountAndSymbol": false,
"RoundingCoefficient": 0,
"DecimalDigits": 2
}]
```