Feature: Looking up currencies

  @AllTests
  Scenario Outline: Looking up Currency Code and Symbol
    When I look up for all Currencies
    Then the Country Code <Country Code> should have symbol <Currency Code>
    Examples:
      | Country Code | Currency Code |
      | AED          | AED           |
