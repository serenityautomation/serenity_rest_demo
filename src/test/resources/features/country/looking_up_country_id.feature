Feature: Looking up country codes

  @AllTests
  Scenario Outline: Looking up Country Id by Country Code
    When I look up destination for country <Country Code>
    Then the resulting CountryId should be <Country ID>
    Examples:
      | Country Code | Country ID |
      | Stockholm    | SE-sky     |