package starter.destination;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DestinationCodeAPI {

    private static String DESTINATION_BY_COUNTRY = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/UK/GBP/en-GB/?query='{country}'";

    @Step("Get destination for country {0}")
    public void fetchDestinationByCountry(String country) {        SerenityRest.given()
            .pathParam("country", country)
            .header("x-rapidapi-key", "9de39692afmsha02c5f42918bef2p16c60ajsn975baebbf045")
            .header("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com")
            .get(DESTINATION_BY_COUNTRY);

    }
}
