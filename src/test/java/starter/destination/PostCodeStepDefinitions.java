package starter.destination;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class PostCodeStepDefinitions {

    @Steps
    DestinationCodeAPI destinationCodeApi;

    @When("I look up destination for country {word}")
    public void lookUpADestination(String country) {
        destinationCodeApi.fetchDestinationByCountry(country);
    }

    @Then("the resulting CountryId should be {}")
    public void theResultingCountryIdShouldBe(String countryId) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(LocationResponse.COUNTRY_ID, equalTo(countryId)));
    }
}
