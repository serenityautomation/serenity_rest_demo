package starter.currency;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class CurrencyCodeAPI {

    private static String CURRENCIES = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/currencies";

    @Step("Get All Available Currencies")
    public void fetchCurrencyByCountry() {
        SerenityRest.given()
            .header("x-rapidapi-key", "9de39692afmsha02c5f42918bef2p16c60ajsn975baebbf045")
            .header("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com")
            .get(CURRENCIES);
    }
}
