package starter.currency;

public class CurrencyResponse {
    public static final String CURRENCY_COUNTRY_CODE = "'Currencies'[0].'Code'";
    public static final String CURRENCY_SYMBOL = "'Currencies'[0].'Symbol'";

}
