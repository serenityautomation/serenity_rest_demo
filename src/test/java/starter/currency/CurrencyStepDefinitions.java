package starter.currency;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.var;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.yecht.Data;
import starter.models.CurrenciesDto;
import starter.models.CurrencyDetail;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class CurrencyStepDefinitions {

    @Steps
    CurrencyCodeAPI currencyCodeAPI;

    @When("I look up for all Currencies")
    public void lookUpACurrency() {
        currencyCodeAPI.fetchCurrencyByCountry();
    }

    @Then("the Country Code {word} should have symbol {word}")
    public void theResultingCountryAndCurrencyCode(String countryCode, String symbol) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        String lastResponse = lastResponse().body().asString();
        CurrenciesDto currencies = objectMapper.readValue(lastResponse, CurrenciesDto.class);
        List<Container> containers = currencies.getCurrencyDetails().stream().map(it -> new Container(it.getCode(), it.getSymbol())).collect(Collectors.toList());

        restAssuredThat(response -> response.statusCode(200));
        Assertions.assertThat(containers).contains(new Container(countryCode, symbol));
    }
}
